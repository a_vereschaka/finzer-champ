package com.avereshchaka.config;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 25 March 2017
 */
public final class Constants {

    public static final String BASE_API_URL = "/api";
    public static final String SPRING_PROFILE_DEV = "dev";
    public static final String SPRING_PROFILE_PROD = "prod";



    private Constants(){}
}
