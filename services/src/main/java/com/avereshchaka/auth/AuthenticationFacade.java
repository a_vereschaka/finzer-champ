package com.avereshchaka.auth;

import org.springframework.security.core.Authentication;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 11 June 2017
 */
public interface AuthenticationFacade {
    Authentication getAuthentication();
}

