package com.avereshchaka.web.rest;

import com.avereshchaka.config.Constants;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 28 May 2017
 */
@RestController
@RequestMapping(path = Constants.BASE_API_URL + "/user")
public class UserRest {

    @RequestMapping("/current")
    public Principal user(Principal principal){
        return principal;
    }
}
