package com.avereshchaka.web.rest;

import com.avereshchaka.config.Constants;
import com.avereshchaka.domain.Category;
import com.avereshchaka.domain.UserTransaction;
import com.avereshchaka.enums.TransactionType;
import com.avereshchaka.repository.CategoryRepository;
import com.avereshchaka.repository.UserTransactionRepository;
import com.avereshchaka.service.CategoryService;
import com.avereshchaka.web.dto.UserTransactionDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 25 March 2017
 */
@RestController
@RequestMapping(path = Constants.BASE_API_URL + "/transaction")
public class UserTransactionRest {

    private UserTransactionRepository userTransactionRepository;

    private CategoryRepository categoryRepository;

    private CategoryService categoryService;

    @Autowired
    public UserTransactionRest(UserTransactionRepository userTransactionRepository, CategoryRepository categoryRepository, CategoryService categoryService) {
        this.userTransactionRepository = userTransactionRepository;
        this.categoryRepository = categoryRepository;
        this.categoryService = categoryService;
    }

    @GetMapping(path = "/transactions")
    public Iterable<UserTransactionDto> getAllUserTransactions(Principal principal){

        Iterable<UserTransaction> all = userTransactionRepository.findAllByUserIdEquals(principal.getName());
        List<UserTransactionDto> result = new ArrayList<>();
        all.forEach((ut) -> {
            UserTransactionDto dto = new UserTransactionDto();
            BeanUtils.copyProperties(ut, dto);

            dto.setCategory(ut.getCategory().getName()); //FIXME
            dto.setCurrency("UAH"); //FIXME
            dto.setTimeInMillis(ut.getTimeInMillis()); // FIXME
            result.add(dto);
        });

        return result;
    }

    @PostMapping(path = "/create")
    public void createNewTransaction(@RequestParam("amount") BigDecimal amount, @RequestParam("category") String categoryName,
                                     @RequestParam(value = "desc", required = false) String description, @RequestParam("type") TransactionType type,
                                     @RequestParam("timestamp") Long timeInMillis,
                                     Principal principal) {

        Category category = categoryService.getOrCreateForCurrentUser(categoryName);

        UserTransaction ut = new UserTransaction();
        ut.setUserId(principal.getName());
        ut.setAmount(amount);
        ut.setCategory(category);
        ut.setDescription(description);
        ut.setType(type);
        ut.setTimeInMillis(timeInMillis);

        userTransactionRepository.save(ut);
    }

}
