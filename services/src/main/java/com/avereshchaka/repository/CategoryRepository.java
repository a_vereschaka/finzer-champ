package com.avereshchaka.repository;

import com.avereshchaka.domain.Category;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 10 June 2017
 */
@Transactional
public interface CategoryRepository extends CrudRepository<Category, Long> {
    Category findByNameAndUserIdEquals(String name, String userId);
}
