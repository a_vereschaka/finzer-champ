package com.avereshchaka.enums;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 25 March 2017
 */
public enum TransactionType {
    INCOME,
    OUTCOME,
    UNKNOWN
}
