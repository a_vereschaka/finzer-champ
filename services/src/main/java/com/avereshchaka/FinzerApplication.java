package com.avereshchaka;

import com.avereshchaka.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class FinzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinzerApplication.class, args);
	}
}
