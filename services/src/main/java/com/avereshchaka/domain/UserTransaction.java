package com.avereshchaka.domain;

import com.avereshchaka.enums.TransactionType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 25 March 2017
 */
@Data
@Entity
public class UserTransaction {

    @Id
    @GeneratedValue
    private Long id;

    private String userId;

    @ManyToOne(cascade = CascadeType.ALL)
    private Category category;

    private BigDecimal amount;

    @ManyToOne(cascade = CascadeType.ALL)
    private Currency currency;

    private String description;

    private @Enumerated TransactionType type;

    private Long timeInMillis;

    @Transient
    @Setter(AccessLevel.NONE)
    private LocalDateTime localDateTime;

    @Setter(AccessLevel.NONE)
    @Transient
    private Money money;

    public UserTransaction() {
    }

    public Money getMoney() {
        if (money == null) {
            money = Money.of(CurrencyUnit.of(currency.getInterCode()), amount);
        }
        return money;
    }

    public LocalDateTime getLocalDateTime(){
        if (localDateTime == null){
            localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(timeInMillis), ZoneId.systemDefault());
        }
        return localDateTime;
    }
}
