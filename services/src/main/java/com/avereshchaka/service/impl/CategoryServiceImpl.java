package com.avereshchaka.service.impl;

import com.avereshchaka.auth.AuthenticationFacade;
import com.avereshchaka.domain.Category;
import com.avereshchaka.repository.CategoryRepository;
import com.avereshchaka.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:artem.vereshchaka@db.com">Artem Vereshchaka</a>
 *         Created: 11 June 2017
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category getOrCreateForCurrentUser(String name) {
        String userId = ((String) authenticationFacade.getAuthentication().getPrincipal());

        Category category = categoryRepository.findByNameAndUserIdEquals(name, userId);

        if (category == null){
            category = new Category();
            category.setUserId(userId);
            category.setName(name);
            categoryRepository.save(category);
        }

        return category;
    }
}
