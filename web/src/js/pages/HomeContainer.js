import React from "react";
import {Segment} from "semantic-ui-react";
import TransactionAddPanel from "../components/TransactionAddPanel";
import TransactionsTable from "../components/TransactionsTable";

export default class HomeContainer extends React.Component {
    render() {
        return (
            <Segment.Group horizontal className="homeContainer">
                <Segment color="purple">
                    <TransactionsTable/>
                </Segment>

                <Segment color="blue">
                    <TransactionAddPanel/>
                </Segment>
            </Segment.Group>
        );
    }
}
