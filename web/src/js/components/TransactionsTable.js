import * as React from "react";
import {Table} from "semantic-ui-react";
import AppUtils from "../utils/AppUtils";

export default class TransactionsTable extends React.Component {

    state = {
        data: []
    };

    componentWillMount(){
        AppUtils.httpGet("transaction/transactions").then((response) => {
            this.setState({
                data: response.data
            });
        });
    }

    convertToDate = (timestamp) => {
        return new Date(timestamp).toLocaleDateString();
    };

    render() {
        const {data} = this.state;

        const listItems = data.map((t, index) =>
            <Table.Row key={index} cells={[this.convertToDate(t.timeInMillis), t.type, t.category, t.description, t.amount, t.currency]}/>
        );
        return (
            <div className="transactionsTable">
                <Table striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Date</Table.HeaderCell>
                            <Table.HeaderCell>Type</Table.HeaderCell>
                            <Table.HeaderCell>Category</Table.HeaderCell>
                            <Table.HeaderCell>Description</Table.HeaderCell>
                            <Table.HeaderCell>Amount</Table.HeaderCell>
                            <Table.HeaderCell>Currency</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {listItems}
                    </Table.Body>
                </Table>
            </div>
        );
    }
}
