import React from "react";
import {Dimmer, Loader, Segment} from "semantic-ui-react";

export default class UiLoader extends React.Component {

    state = {isHidden: true};

    show() {
        this.setState({isHidden: false});
    }

    hide() {
        this.setState({isHidden: true});
    }

    render() {
        const {isHidden} = this.state;
        return (
            <div hidden={isHidden}>
                <Segment>
                    <Dimmer active>
                        <Loader size='massive' content='Loading'/>
                    </Dimmer>
                </Segment>
            </div>
        )
    }
}