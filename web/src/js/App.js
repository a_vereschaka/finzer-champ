import * as React from "react";
import MainMenu from "./pages/MainMenu";
import HomeContainer from "./pages/HomeContainer";
import AppUtils from "./utils/AppUtils";
import LoginContainer from "./pages/LoginContainer";
import StatisticsContainer from "./pages/StatisticsContainer";

export default class App extends React.Component {

    state = {
        authenticated: false,
        screen: <HomeContainer/>
    };

    screens() {
        return (
            <div>
                <MainMenu onChange={this.changeScreen}/>
                {this.state.screen}
            </div>
        )
    }

    loginForm() {
        return (
            <div>
                Hi, Anonymous! <br/>
                <LoginContainer/>
            </div>
        );
    }

    changeScreen = (name) => {
        if (name === "Home"){
            this.setState({
                screen: <HomeContainer/>
            })
        } else if (name === "Statistics"){
            this.setState({
                screen: <StatisticsContainer/>
            })
        }
    }


    componentWillMount() {
        let that = this;
        AppUtils.httpGetCustom('user/current')
            .then(function (response) {
                that.setState({
                    user: response.data.userAuthentication.details.name,
                    authenticated: true
                });
            })
            .catch(function (error) {
                that.setState({
                    user: 'N/A',
                    authenticated: false
                });
            });
    }

    render() {
        return (
            <div>
                { this.state.authenticated ? this.screens() : this.loginForm()}
            </div>
        )
    }
}
