import axios from "axios";
import Moment from "moment";

const API_CONTEXT = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2)) + '/api/';

function httpHeader() {
    return {headers: {'X-Requested-With': 'XMLHttpRequest'}};
}

function transformMomentToTimestamp(object) {
    if (typeof object === 'object') {
        for (var property in object) {
            var item = object[property];
            if (Moment.isMoment(item)) {
                object[property] = {
                    "type": "timestamp",
                    "timestamp": item.unix()
                };
            } else {
                transformMomentToTimestamp(item);
            }
        }
    }
}

function onError(response, validationCheck) {
    AppUtils.hideLoading();

    window.alert(response.status);
}

export default class AppUtils {

    //REST Helpers----------------------
    static httpGet(url) {
        AppUtils.showLoading();

        return axios.get(API_CONTEXT + url, httpHeader()).catch(onError).then(response => {
            AppUtils.hideLoading();
            return response
        });
    }

    static httpGetCustom(url){
        return axios.get(API_CONTEXT + url);
    }

    static httpGetFilter(url, json) {
        AppUtils.showLoading();
        //TODO add url params
        return axios.get(API_CONTEXT + url, httpHeader()).catch(onError).then(response => {
            AppUtils.hideLoading();
            return response
        });
    }

    static httpDelete(url, validationCheck) {
        AppUtils.showLoading();

        return axios.delete(API_CONTEXT + url, httpHeader()).catch(response => onError(response, validationCheck)).then(response => {
            AppUtils.hideLoading();
            return response
        });
    }

    static httpPost(url, json, validationCheck) {
        AppUtils.showLoading();

        transformMomentToTimestamp(json);

        return axios.post(API_CONTEXT + url, json, httpHeader()).catch(response => onError(response, validationCheck)).then(response => {
            AppUtils.hideLoading();
            return response
        });
    }

    static httpPut(url, json, validationCheck) {
        AppUtils.showLoading();

        transformMomentToTimestamp(json);

        return axios.put(API_CONTEXT + url, json, httpHeader()).catch(response => onError(response, validationCheck)).then(response => {
            AppUtils.hideLoading();
            return response
        });
    }

    //Loading indicator
    static showLoading() {
        if (!this.loaders) {
            this.loaders = 0;
        }
        this.loaders++;
    }

    static hideLoading() {
        this.loaders--;
        if (this.loaders <= 0) {
            //TODO loader
        }
    }

}
