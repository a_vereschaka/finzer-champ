'use strict';

var path = require('path');
var webpack = require('webpack');
let _ = require('lodash');
let FileSystem = require("fs");

let baseConfig = require('./base').config;
let srcPath = require('./base').srcPath;
let publicPath = require('./base').publicPath;
let buildPath = require('./base').buildPath;

let config = _.merge({
    devtool: 'hidden-source-map',
    output: {
        path: path.join(__dirname, buildPath),
        filename: '[name].[chunkhash].js',
        chunkFilename: "[id].app.[chunkhash].js",
        publicPath: ''
    },
    entry: {
        app: [
            './src/js/index'
        ]
    },
    plugins: baseConfig.plugins.concat([
        function () {
            this.plugin("done", function (statsData) {
                var stats = statsData.toJson();

                console.log(stats.assetsByChunkName.vendor);

                if (!stats.errors.length) {
                    var htmlFileName = srcPath + "/index.html";
                    var html = FileSystem.readFileSync(path.join(__dirname, htmlFileName), "utf8");

                    var htmlOutput = html.replace('vendor.js', stats.assetsByChunkName.vendor)
                        .replace('app.js', stats.assetsByChunkName.app)
                        .replace('manifest.js', stats.assetsByChunkName.manifest);

                    FileSystem.writeFileSync(
                        path.join(__dirname, '/../build/', "index.html"),
                        htmlOutput);
                }
            });
        },
        new webpack.optimize.CommonsChunkPlugin({
            names: ["vendor", "manifest"],
            filenames: ['vendor.[hash].js', 'manifest.[hash].js']
        }),
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            output: {
                comments: false
            },
            sourceMap: false
        })
    ])
}, baseConfig);

module.exports = config;

