'use strict';
let path = require('path');
let webpack = require('webpack');
let _ = require('lodash');

let baseConfig = require('./base').config;
let publicPath = require('./base').publicPath;
let buildPath = require('./base').buildPath;


let config = _.merge({
    devtool: 'cheap-module-eval-source-map',
    output: {
        path: path.join(__dirname, buildPath),
        filename: 'app.js',
        publicPath: publicPath
    },
    entry: {
        js: [
            'webpack-hot-middleware/client',
            './src/js/index'
        ]
    },
    plugins: baseConfig.plugins.concat([
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: Infinity,
            filename: 'vendor.js'
        }),
        new webpack.HotModuleReplacementPlugin()
    ])
}, baseConfig);

module.exports = config;
